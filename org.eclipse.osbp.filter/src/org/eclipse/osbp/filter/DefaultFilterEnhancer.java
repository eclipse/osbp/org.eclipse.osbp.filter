/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.filter;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.osbp.jpa.services.Query;
import org.eclipse.osbp.jpa.services.filters.LAnd;
import org.eclipse.osbp.jpa.services.filters.LIn;
import org.eclipse.osbp.jpa.services.filters.LNot;
import org.eclipse.osbp.runtime.common.filter.IFilterEnhancer;
import org.eclipse.osbp.runtime.common.filter.IQuery;
import org.eclipse.osbp.ui.api.user.filter.FilterMap;
import org.eclipse.osbp.ui.api.useraccess.IUserAccessService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class DefaultFilterEnhancer.
 */
@Component(immediate = true)
public class DefaultFilterEnhancer implements IFilterEnhancer {

	/** The user access service. */
	private IUserAccessService userAccessService;

	/** The logger. */
	private final Logger LOGGER = LoggerFactory.getLogger("servicebinder");

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.dsl.dto.lib.services.filters.IFilterEnhancer#
	 * enhanceQuery(org.eclipse.osbp.dsl.dto.lib.services.IQuery)
	 */
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void enhanceQuery(IQuery query) {
		if (userAccessService != null && userAccessService.isAuthenticated()) {
			FilterMap fm = userAccessService.getFilterMap();
			if ( fm == null ) return;
			
			List<Class> queryMap = (List<Class>) query.getMap().get(Query.ENTITY_CLASSES_KEY);
			if (queryMap != null) {
				LIn inFilter = null;
				LNot notFilter = null;
				for (Class entityClass : queryMap) {
					String propertyId = "id";
					// LIn filter creation
					List<String> filterInValues = fm.getInFilterMap()
							.get(entityClass.getName());
					if (filterInValues != null) {
						List<Comparable<?>> values = new ArrayList<>();
						for (Comparable<?> value : filterInValues) {
							values.add(value);
						}
						inFilter = new LIn(propertyId, values);
					}
					// LNot filter creation
					List<String> filterNotValues = fm.getNotFilterMap()
							.get(entityClass.getName());
					if (filterNotValues != null) {
						List<Comparable<?>> values = new ArrayList<>();
						for (Comparable<?> value : filterNotValues) {
							values.add(value);
						}
						notFilter = new LNot(new LIn(propertyId, values));
					}
					// ReplaceFilter
					if ((inFilter != null) && (notFilter != null)) {
						query.replaceFilter(new LAnd(inFilter, notFilter));
					} else if (inFilter != null) {
						query.replaceFilter(inFilter);
					} else if (notFilter != null) {
						query.replaceFilter(notFilter);
					}
				}
			}
		}
	}

	/**
	 * Bind user access service.
	 *
	 * @param userAccessService
	 *            the user access service
	 */
	@Reference(cardinality = ReferenceCardinality.MANDATORY)
	public synchronized void bindUserAccessService(final IUserAccessService userAccessService) {
		this.userAccessService = userAccessService;
		LOGGER.debug("DefaultFilterEnhancer UserAcccessService bound");
	}

	/**
	 * Unbind user access service.
	 *
	 * @param userAccessService
	 *            the user access service
	 */
	public synchronized void unbindUserAccessService(final IUserAccessService userAccessService) { // NOSONAR
		this.userAccessService = null;
		LOGGER.debug("DefaultFilterEnhancer UserAcccessService unbound");
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.runtime.common.filter.IFilterEnhancer#checkGet(java.lang.Class, java.lang.Object)
	 */
	@Override
	public boolean checkGet(Class<?> entityClass, Object id) {
		if (userAccessService != null && userAccessService.isAuthenticated()) {
			
			FilterMap fm = userAccessService.getFilterMap();
			if (fm == null) return true;

			List<String> filterInValues = fm.getInFilterMap().get(entityClass.getName());
			if (filterInValues != null) {
				if(id instanceof String) {
					return (filterInValues.contains(id));
				} else {
					return (filterInValues.contains(new Integer((int) id).toString()));
				}
			}
			List<String> filterNotValues = fm.getNotFilterMap()
					.get(entityClass.getName());
			if (filterNotValues != null) {
				if(id instanceof String) {
					return !(filterNotValues.contains(id));
				} else {
					return !(filterNotValues.contains(new Integer((int) id).toString()));
				}
			}
		}
		return true;
	}

}
